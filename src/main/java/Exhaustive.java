
import java.util.ArrayList;
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this sub1late
 */
/**
 *
 * @author Acer
 */
public class Exhaustive {

    private int[] arr;
    static int Index = 0;
    static ArrayList<ArrayList<Integer>> subArr1 = new ArrayList<ArrayList<Integer>>();
    static ArrayList<ArrayList<Integer>> subArr2 = new ArrayList<ArrayList<Integer>>();

    public Exhaustive(int[] arr) {
        this.arr = arr;
    }

    static void prepareArray(int arr[], int n, int r) {
        int data[] = new int[r];
        Add(arr, data, 0, n - 1, 0, r);
    }

    static void Add(int arr[], int data[], int start, int end, int index, int r) {
        ArrayList<Integer> temp = new ArrayList<>();
        if (index == r) {
            for (int j = 0; j < r; j++) {
                temp.add(data[j]);
            }
            subArr1.add(Index, temp);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= r - index; i++) {
            data[index] = arr[i];
            Add(arr, data, i + 1, end, index + 1, r);
        }
    }

    public void process() {
        ArrayList<Integer> sub1 = new ArrayList<>();
        ArrayList<Integer> sub2 = new ArrayList<>();
        for (int x = 0; x < arr.length; x++) {
            sub1.add(arr[x]);
        }
        for (int y = 0; y < arr.length; y++) {
            sub2.add(arr[y]);
        }
        int arr2[] = arr;
        int n = arr.length;

        for (int i = arr.length; i >= 0; i--) {
            prepareArray(arr, n, i);
        }
        Index = 1;
        subArr2.add(0, sub2);
        for (int j = 1; j < subArr1.size(); j++) {
            sub1 = new ArrayList<>();
            for (int i = 0; i < arr.length; i++) {
                sub1.add(arr[i]);
            }
            for (int k = 0; k < subArr1.get(j).size(); k++) {
                for (int u = 0; u < sub1.size(); u++) {
                    if (sub1.get(u) == subArr1.get(j).get(k)) {
                        sub1.remove(u);
                        u--;
                    } } }
            subArr2.add(Index, sub1);
            Index++;
        }
    }

    public void ShowArray1() {
        for (int i = 0; i < subArr1.size(); i++) {
            for (int j = 0; j < subArr1.get(i).size(); j++) {
                System.out.print(subArr1.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }
    public void ShowArray2() {
        for (int i = 0; i < subArr2.size(); i++) {
            for (int j = 0; j < subArr2.get(i).size(); j++) {
                System.out.print(subArr2.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }
    
    public void sum() {
        for (int i = 0; i < subArr1.size(); i++) {
            if (subArr1.contains(subArr2.get(i))) {
                subArr2.remove(i);
                subArr1.remove(i);
            }
        }
        for (int i = 0; i < subArr1.size(); i++) {
            int sum1 = 0;
            int sum2 = 0;
            for (int j = 0; j < subArr1.get(i).size(); j++) {
                sum1+= subArr1.get(i).get(j);
            }
            for (int j = 0; j < subArr2.get(i).size(); j++) {
                sum2 += subArr2.get(i).get(j);
            }
            if (sum1 == sum2) {
                System.out.println(subArr1.get(i) + " " + subArr2.get(i));
            }
        }
    }
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int num = kb.nextInt();
        int a[] = new int[num];
        for (int i = 0; i < num; i++) {
            a[i] = kb.nextInt();
        }
        Exhaustive exs = new Exhaustive(a);
        exs.process();
        exs.sum();
    }
}
